<?php 

    $botToken = $_ENV["token"];


    $adminid = "6269386399";
    $admin = "vkripz";
    $botname = "CryptoEarnWorkBot";
    $cur = "TRX";
    $bonus_time = 86400;
    $per_refer = number_format(floatval(0.1),2,".","");
    $channel = "";
    $channelid = "";
    
    $website = "https://api.telegram.org/bot".$botToken; 
    $update = url_get_contents('php://input');
    $update = file_get_contents('php://input');
    $update = json_decode($update, TRUE);

    $message = $update["message"]["text"];
    $userid = $update["message"]["chat"]["id"];
    $userfirst_name = $update["message"]["chat"]["first_name"];
    $fromid = $update["message"]["from"]["id"];
    $first_name = $update["message"]["from"]["first_name"];
    $username = $update["message"]["from"]["username"];
    $chat = $update["message"]["chat"]["title"];
    $type = $update["message"]["chat"]["type"];
    $chatusername = $update["message"]["chat"]["username"];

    $callback_query = $update["callback_query"];
    $alert_id = $update["callback_query"]["id"];
    $data = $callback_query["data"];
    $action = $update["callback_query"]["data"];
    $message_id = $callback_query["message"]["message_id"]; 
    $chatid = $update["callback_query"]["message"]["chat"]["id"];   
    $qqchatid = $update["callback_query"]["from"]["id"]; 
    $qqname = $update["callback_query"]["from"]["username"];  
    $chatname = $update["callback_query"]["from"]["first_name"];   
    $alert_id = $update["callback_query"]["id"];

    $refid = explode("/start " , $message)[1];
    $ex = explode(" " , $message);


    if(isset($message)){

        if($type !== "private"){
            return;
        }

        if($message === "/start" || $message === "/start $refid"){
            $db = json_decode(file_get_contents("db.json"));
            if(empty($db->$userid)){
                $db->$userid = (object) [first_name => $first_name, username => $username, inviter => $refid, balance => 0, invites => 0];
                $db->$refid->invites = intval($db->$refid->invites) + 1;
                $db->$refid->balance = intval($db->$refid->balance) + $per_refer;
                file_put_contents("db.json", json_encode($db));
                sendMessage($adminid, "<b>New User: $first_name</b>");
            }
            $key = ["keyboard" => [[["text" => "Balance"]]]];
            sendMessage($userid, "Welcome $first_name", $key);
            return;
        }

    }


    function sendMessage ($userid, $text,$key=null) {
        $args = array( "chat_id" => $userid, "text" => $text, "parse_mode" => "html", "reply_markup" => $key ? json_encode($key) : $key );
        $url = $GLOBALS[website]."/sendMessage?disable_web_page_preview=True&".http_build_query($args); 
        $response = json_decode(url_get_contents($url));
        return $response;
    } 

    function sendPhoto ($userid, $photo, $caption=null, $key=null) {
        $args = array( "chat_id" => $userid, "photo" => $photo, "caption" => $caption, "parse_mode" => "html", "reply_markup" => $key ? json_encode($key) : $key );
        $url = $GLOBALS[website]."/sendPhoto?disable_web_page_preview=True&".http_build_query($args); 
        $response = json_decode(url_get_contents($url));
        return $response;
    } 

    function editMessage ($userid, $message_id, $text, $key=null) {
        $args = array( "chat_id" => $userid, "message_id" => $message_id, "text" => $text, "parse_mode" => "html", "reply_markup" => $key ? json_encode($key) : $key );
        $url = $GLOBALS[website]."/editMessageText?disable_web_page_preview=True&".http_build_query($args); 
        $response = json_decode(url_get_contents($url));
        return $response;
    }
        
    function deleteMessage($userid, $message_id) { 
        $args = array("chat_id" => $userid, "message_id" => $message_id,  "parse_mode" => "HTML" );
        $url = $GLOBALS[website]."/deleteMessage?disable_web_page_preview=True&".http_build_query($args); 
        $response = json_decode(url_get_contents($url));
        return $response;
    }
        
    function getChatMember($userid , $chatid) { 
        $args = array("chat_id" => $chatid, "user_id" => $userid );
        $url = $GLOBALS[website]."/getChatMember?disable_web_page_preview=True&".http_build_query($args); 
        return json_decode(url_get_contents($url))->result->status;
    }
        
    
    function topAlert($qchatid, $alert_id , $text) { 
        $args = array( "chat_id" => $qchatid, "text" => $text, "parse_mode" => "HTML" );
        $url = $GLOBALS[website]."/answerCallbackQuery?callback_query_id=$alert_id&disable_web_page_preview=True&".http_build_query($args); 
        $response = json_decode(url_get_contents($url));
        return $response;
    }


    function url_get_contents($Url) {
        if(!function_exists('curl_init')) {
            die('CURL is not installed!'); 
        }
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $Url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        curl_close($ch); 
        return $output; 
    } 
          
?>
